# Project Management System

The application which helps in storage invoices.


## Build the app

### Start PostgreSQL in Docker

To run database in docker execute:
```
docker run -d --name postgres-ia -p 5432:5432 -e POSTGRES_DB=invoice -e POSTGRES_PASSWORD=postgres -e POSTGRES_USER=postgres postgres
```

## Start Up

The running PostgreSQL is needed to start the app.

To initialize the database with sample data, execute the command(updated with correct data):

```
psql -h localhost -p 5432 -U postgres < invoice.dmp
```

Next, run the spring-boot app:

```
mvn clean spring-boot:run
```

Last step is run the angular app. In invoice-archiver-web folder execute:

```
npm install
ng serve
```

The running app will be available in [localhost:8080](http://localhost:8080) and [localhost:4200](http://localhost:4200)

## Start Up with Docker-Compose

For starting the app with docker-compose just execute:

```
docker-compose up -d
```

The running app will be available in [localhost:8080](http://localhost:8080) and [localhost:4200](http://localhost:4200)

## Author

* **Artur Siembab** - [GitHub](https://github.com/ajtuss), [linkedIn](https://www.linkedin.com/in/artur-siembab/)
