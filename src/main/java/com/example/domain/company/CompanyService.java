package com.example.domain.company;

import com.example.domain.contractor.Contractor;
import com.example.domain.contractor.ContractorDTO;
import com.example.domain.contractor.ContractorRepository;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class CompanyService {

    private final CompanyRepository companyRepository;
    private final ContractorRepository contractorRepository;
    private final ModelMapper modelMapper;
    private Logger logger = LoggerFactory.getLogger(CompanyService.class);


    @Autowired
    public CompanyService(CompanyRepository companyRepository, ContractorRepository contractorRepository, ModelMapper modelMapper) {
        this.companyRepository = companyRepository;
        this.contractorRepository = contractorRepository;
        this.modelMapper = modelMapper;
    }


    @Transactional(readOnly = true)
    public List<CompanyDTO> getAll() {
        logger.debug("Request to get all Companies");

        return companyRepository.findAll()
                                .stream()
                                .map(company -> modelMapper.map(company, CompanyDTO.class))
                                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public Optional<CompanyDTO> getOne(long id) {
        logger.debug("Request to get Company : {}", id);

        return companyRepository.findById(id)
                                .map(company -> modelMapper.map(company, CompanyDTO.class));
    }

    public CompanyDTO save(CompanyDTO companyDTO) {
        logger.debug("Request to save Company : {}", companyDTO);

        Company company = modelMapper.map(companyDTO, Company.class);
        if (company.getId() != null) {
            companyRepository.findById(company.getId())
                             .ifPresent(oldCompany -> company.setContractors(oldCompany.getContractors()));
        }

        Company saved = companyRepository.save(company);
        return modelMapper.map(saved, CompanyDTO.class);
    }

    @Transactional(readOnly = true)
    public boolean existsById(long idValue) {
        logger.debug("Request to existById Company : {}", idValue);

        return companyRepository.existsById(idValue);
    }

    @Transactional(readOnly = true)
    public Optional<CompanyDTO> findByNip(String nip) {
        logger.debug("Request to findByNip Company : {}", nip);

        return companyRepository.findByNip(nip)
                                .map(company -> modelMapper.map(company, CompanyDTO.class));
    }

    public void delete(long id) {
        logger.debug("Request to delete Company : {}", id);

        companyRepository.findById(id)
                         .ifPresent(companyRepository::delete);
    }

    @Transactional(readOnly = true)
    public Optional<CompanyDTO> findByShortName(String shortName) {
        logger.debug("Request to findByShortName Company : {}", shortName);

        return companyRepository.findByShortName(shortName)
                                .map(company -> modelMapper.map(company, CompanyDTO.class));
    }

    public boolean addContractorToCompany(long companyId, long contractorId) {
        logger.debug("Request to add Contractor : {} to Company : {}", contractorId, companyId);

        Optional<Contractor> contractorOptional = contractorRepository.findById(contractorId);
        Optional<Company> companyOptional = companyRepository.findById(companyId);

        contractorOptional.ifPresent(contractor -> {
            companyOptional.map(company -> company.addContractor(contractor))
                           .ifPresent(companyRepository::save);
        });

        return contractorOptional.isPresent() && companyOptional.isPresent();
    }

    public boolean addContractorToCompany(long companyId, String contractorShortName) {
        logger.debug("Request to add Contractor : {} to Company : {}", contractorShortName, companyId);

        Optional<Contractor> contractorOptional = contractorRepository.findByShortName(contractorShortName);
        Optional<Company> companyOptional = companyRepository.findById(companyId);

        contractorOptional.ifPresent(contractor -> {
            companyOptional.map(company -> company.addContractor(contractor))
                           .ifPresent(companyRepository::save);
        });

        return contractorOptional.isPresent() && companyOptional.isPresent();
    }

    public List<ContractorDTO> getAllContractorsByCompanyId(long id, String filter) {
        logger.debug("Request to get all Contractors from Company : {}", id);

        if (filter == null) {
            return contractorRepository.findRelatedByCompany(id)
                                       .stream()
                                       .map(contractor -> modelMapper.map(contractor, ContractorDTO.class))
                                       .collect(Collectors.toList());
        }
        return contractorRepository.findRelatedByCompany(id, filter)
                                   .stream()
                                   .map(contractor -> modelMapper.map(contractor, ContractorDTO.class))
                                   .collect(Collectors.toList());
    }

    public boolean deleteContractorFromCompany(long id, ContractorDTO contractorDTO) {
        logger.debug("Request to delete Contractor : {} from Company : {}", contractorDTO.getId(), id);

        Contractor contractor = modelMapper.map(contractorDTO, Contractor.class);
        return companyRepository.findById(id)
                                .map(company -> company.deletContractor(contractor))
                                .isPresent();

    }
}
