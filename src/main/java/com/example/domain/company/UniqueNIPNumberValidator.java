package com.example.domain.company;

import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Optional;

class UniqueNIPNumberValidator implements ConstraintValidator<UniqueNIPNumber, CompanyDTO> {

    @Autowired
    private CompanyService companyService;


    @Override
    public void initialize(UniqueNIPNumber constraintAnnotation) {

    }

    @Override
    public boolean isValid(CompanyDTO value, ConstraintValidatorContext context) {
        if (value == null) {
            return true;
        }
        Long id = value.getId();
        String nip = value.getNip();

        Optional<CompanyDTO> found = companyService.findByNip(nip);
        if (!found.isPresent() || found.get().getId().equals(id)) {
            return true;
        }
        context.disableDefaultConstraintViolation();
        context.buildConstraintViolationWithTemplate(context.getDefaultConstraintMessageTemplate())
               .addPropertyNode("nip").addConstraintViolation();
        return false;
    }

}
