package com.example.domain.company;

import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Optional;

class UniqueShortNameValidator implements ConstraintValidator<UniqueShortName, CompanyDTO> {

    @Autowired
    private CompanyService companyService;


    @Override
    public void initialize(UniqueShortName constraintAnnotation) {
    }

    @Override
    public boolean isValid(CompanyDTO value, ConstraintValidatorContext context) {
        if (value == null) {
            return true;
        }
        Long id = value.getId();
        String shortName = value.getShortName();

        Optional<CompanyDTO> found = companyService.findByShortName(shortName);
        if (!found.isPresent() || found.get().getId().equals(id)) {
            return true;
        }
        context.disableDefaultConstraintViolation();
        context.buildConstraintViolationWithTemplate(context.getDefaultConstraintMessageTemplate())
               .addPropertyNode("shortName").addConstraintViolation();
        return false;
    }

}
