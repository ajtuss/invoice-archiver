package com.example.domain.invoice;

import com.example.constraints.CreateEntityGroup;
import com.example.constraints.UpdateEntityGroup;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

public class InvoiceDTO {

    @NotNull(groups = UpdateEntityGroup.class)
    @Null(groups = CreateEntityGroup.class)
    private Long id;
    @NotNull
    private String invoiceNumber;
    @NotNull
    private LocalDate issueDate;
    @NotNull
    private LocalDate dueDate;
    @NotNull
    private BigDecimal netTotalAmount;
    @NotNull
    private BigDecimal taxTotalAmount;
    @NotNull
    private BigDecimal grossTotalAmount;
    @NotNull
    @CompanyExist
    private Long companyId;
    private String companyShortName;
    @NotNull
    @ContractorExist
    private Long contractorId;
    private String contractorShortName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public LocalDate getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(LocalDate issueDate) {
        this.issueDate = issueDate;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDate dueDate) {
        this.dueDate = dueDate;
    }

    public BigDecimal getNetTotalAmount() {
        return netTotalAmount;
    }

    public void setNetTotalAmount(BigDecimal netTotalAmount) {
        this.netTotalAmount = netTotalAmount;
    }

    public BigDecimal getTaxTotalAmount() {
        return taxTotalAmount;
    }

    public void setTaxTotalAmount(BigDecimal taxTotalAmount) {
        this.taxTotalAmount = taxTotalAmount;
    }

    public BigDecimal getGrossTotalAmount() {
        return grossTotalAmount;
    }

    public void setGrossTotalAmount(BigDecimal grossTotalAmount) {
        this.grossTotalAmount = grossTotalAmount;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public String getCompanyShortName() {
        return companyShortName;
    }

    public void setCompanyShortName(String companyShortName) {
        this.companyShortName = companyShortName;
    }

    public Long getContractorId() {
        return contractorId;
    }

    public void setContractorId(Long contractorId) {
        this.contractorId = contractorId;
    }

    public String getContractorShortName() {
        return contractorShortName;
    }

    public void setContractorShortName(String contractorShortName) {
        this.contractorShortName = contractorShortName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InvoiceDTO that = (InvoiceDTO) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(invoiceNumber, that.invoiceNumber) &&
                Objects.equals(issueDate, that.issueDate) &&
                Objects.equals(dueDate, that.dueDate) &&
                Objects.equals(netTotalAmount, that.netTotalAmount) &&
                Objects.equals(taxTotalAmount, that.taxTotalAmount) &&
                Objects.equals(grossTotalAmount, that.grossTotalAmount) &&
                Objects.equals(companyId, that.companyId) &&
                Objects.equals(contractorId, that.contractorId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, invoiceNumber, issueDate, dueDate, netTotalAmount, taxTotalAmount, grossTotalAmount, companyId, contractorId);
    }

    @Override
    public String toString() {
        return "InvoiceDTO{" +
                "id=" + id +
                ", invoiceNumber='" + invoiceNumber + '\'' +
                ", issueDate=" + issueDate +
                ", dueDate=" + dueDate +
                ", netTotalAmount=" + netTotalAmount +
                ", taxTotalAmount=" + taxTotalAmount +
                ", grossTotalAmount=" + grossTotalAmount +
                ", companyId=" + companyId +
                ", contractorId=" + contractorId +
                '}';
    }
}
