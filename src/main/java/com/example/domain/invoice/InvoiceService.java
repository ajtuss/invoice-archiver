package com.example.domain.invoice;

import com.example.domain.company.CompanyRepository;
import com.example.domain.contractor.ContractorRepository;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class InvoiceService {

    private final InvoiceRepository invoiceRepository;
    private final CompanyRepository companyRepository;
    private final ContractorRepository contractorRepository;
    private final ModelMapper modelMapper;
    private Logger logger = LoggerFactory.getLogger(InvoiceService.class);

    @Autowired
    public InvoiceService(InvoiceRepository invoiceRepository, CompanyRepository companyRepository, ContractorRepository contractorRepository, ModelMapper modelMapper) {
        this.invoiceRepository = invoiceRepository;
        this.companyRepository = companyRepository;
        this.contractorRepository = contractorRepository;
        this.modelMapper = modelMapper;
    }


    @Transactional(readOnly = true)
    public List<InvoiceDTO> getAll() {
        logger.debug("Request to get all Invoices");

        return invoiceRepository.findAll(new Sort("issueDate"))
                                .stream()
                                .map(invoice -> modelMapper.map(invoice, InvoiceDTO.class))
                                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public Optional<InvoiceDTO> getOne(long id) {
        logger.debug("Request to get Invoice : {}", id);

        return invoiceRepository.findById(id)
                                .map(invoice -> modelMapper.map(invoice, InvoiceDTO.class));
    }

    public InvoiceDTO save(InvoiceDTO invoiceDTO) {
        logger.debug("Request to save Invoice : {}", invoiceDTO);

        Invoice invoice = modelMapper.map(invoiceDTO, Invoice.class);
        invoice.setCompany(companyRepository.getOne(invoiceDTO.getCompanyId()));
        invoice.setContractor(contractorRepository.getOne(invoiceDTO.getContractorId()));
        Invoice saved = invoiceRepository.save(invoice);
        return modelMapper.map(saved, InvoiceDTO.class);
    }

    public void delete(long id) {
        logger.debug("Request to delete Invoice : {}", id);

        invoiceRepository.findById(id)
                         .ifPresent(invoiceRepository::delete);
    }
}
