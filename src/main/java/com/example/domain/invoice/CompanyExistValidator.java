package com.example.domain.invoice;

import com.example.domain.company.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

class CompanyExistValidator implements ConstraintValidator<CompanyExist, Long> {

    @Autowired
    private CompanyService companyService;

    @Override
    public void initialize(CompanyExist companyExist) {
    }

    @Override
    public boolean isValid(Long company, ConstraintValidatorContext context) {
        if (company != null) {
            return companyService.existsById(company);
        }
        return false;
    }
}
