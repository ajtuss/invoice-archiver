package com.example.domain.invoice;

import com.example.domain.contractor.ContractorService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

class ContractorExistValidator implements ConstraintValidator<ContractorExist, Long> {

    @Autowired
    private ContractorService contractorService;

    @Override
    public void initialize(ContractorExist contractorExist) {
    }

    @Override
    public boolean isValid(Long contractorId, ConstraintValidatorContext context) {
        if (contractorId != null) {
            return contractorService.existsById(contractorId);
        }
        return false;
    }
}
