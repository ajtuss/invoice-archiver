package com.example.domain.contractor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ContractorRepository extends JpaRepository<Contractor, Long> {
    Optional<Contractor> findByNip(String nip);

    Optional<Contractor> findByShortName(String shortName);

    @Query("SELECT c FROM Contractor c WHERE c.shortName LIKE %:search%")
    List<Contractor> findAllByName(@Param("search") String search);

    @Query("SELECT cr FROM Contractor cr JOIN cr.companies co WHERE co.id = :id AND cr.shortName LIKE %:filter%")
    List<Contractor> findRelatedByCompany(@Param("id") long id, @Param("filter") String filter );

    @Query("SELECT cr FROM Contractor cr JOIN cr.companies co WHERE co.id = :id")
    List<Contractor> findRelatedByCompany(@Param("id") long id);

}
