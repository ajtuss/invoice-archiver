package com.example.domain.contractor;

import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Optional;

class UniqueShortNameValidator implements ConstraintValidator<UniqueShortName, ContractorDTO> {

    @Autowired
    private ContractorService contractorService;


    @Override
    public void initialize(UniqueShortName constraintAnnotation) {
    }

    @Override
    public boolean isValid(ContractorDTO value, ConstraintValidatorContext context) {
        if (value == null) {
            return true;
        }
        Long id = value.getId();
        String shortName = value.getShortName();

        Optional<ContractorDTO> found = contractorService.findByShortName(shortName);
        if (!found.isPresent() || found.get().getId().equals(id)) {
            return true;
        }
        context.disableDefaultConstraintViolation();
        context.buildConstraintViolationWithTemplate(context.getDefaultConstraintMessageTemplate())
               .addPropertyNode("shortName").addConstraintViolation();
        return false;
    }

}
