package com.example.domain.contractor;

import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Optional;

class UniqueNIPNumberValidator implements ConstraintValidator<UniqueNIPNumber, ContractorDTO> {

    @Autowired
    private ContractorService contractorService;


    @Override
    public void initialize(UniqueNIPNumber constraintAnnotation) {

    }

    @Override
    public boolean isValid(ContractorDTO value, ConstraintValidatorContext context) {
        if (value == null) {
            return true;
        }
        Long id = value.getId();
        String nip = value.getNip();

        Optional<ContractorDTO> found = contractorService.findByNip(nip);
        if (!found.isPresent() || found.get().getId().equals(id)) {
            return true;
        }
        context.disableDefaultConstraintViolation();
        context.buildConstraintViolationWithTemplate(context.getDefaultConstraintMessageTemplate())
               .addPropertyNode("nip").addConstraintViolation();
        return false;
    }

}
