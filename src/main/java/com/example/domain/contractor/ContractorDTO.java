package com.example.domain.contractor;

import com.example.constraints.CreateEntityGroup;
import com.example.constraints.UpdateEntityGroup;
import org.hibernate.validator.constraints.pl.NIP;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.util.Objects;

@UniqueNIPNumber
public class ContractorDTO {

    @NotNull(groups = UpdateEntityGroup.class)
    @Null(groups = CreateEntityGroup.class)
    private Long id;

    @NotNull
    private String shortName;

    private String fullName;

    private String city;

    private String zip;

    private String street;

    @NotNull
    @Email(message = "invalid")
    private String email;

    @NotNull
    @NIP(message = "invalid")
    private String nip;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ContractorDTO that = (ContractorDTO) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(shortName, that.shortName) &&
                Objects.equals(fullName, that.fullName) &&
                Objects.equals(city, that.city) &&
                Objects.equals(zip, that.zip) &&
                Objects.equals(street, that.street) &&
                Objects.equals(email, that.email) &&
                Objects.equals(nip, that.nip);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, shortName, fullName, city, zip, street, email, nip);
    }

    @Override
    public String toString() {
        return "ContractorDTO{" +
                "id=" + id +
                ", shortName='" + shortName + '\'' +
                ", fullName='" + fullName + '\'' +
                ", city='" + city + '\'' +
                ", zip='" + zip + '\'' +
                ", street='" + street + '\'' +
                ", email='" + email + '\'' +
                ", nip='" + nip + '\'' +
                '}';
    }
}
