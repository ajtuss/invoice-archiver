package com.example.domain.contractor;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class ContractorService {

    private final ContractorRepository contractorRepository;
    private final ModelMapper modelMapper;
    private Logger logger = LoggerFactory.getLogger(ContractorService.class);

    @Autowired
    public ContractorService(ContractorRepository companyRepository, ModelMapper modelMapper) {
        this.contractorRepository = companyRepository;
        this.modelMapper = modelMapper;
    }

    @Transactional(readOnly = true)
    public List<ContractorDTO> getAll(String search) {
        logger.debug("Request to get all Contractors by Name : {}", search);

        return contractorRepository.findAllByName(search)
                                   .stream()
                                   .map(contractor -> modelMapper.map(contractor, ContractorDTO.class))
                                   .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public List<ContractorDTO> getAll() {
        logger.debug("Request to get all Contractors");

        return contractorRepository.findAll()
                                   .stream()
                                   .map(contractor -> modelMapper.map(contractor, ContractorDTO.class))
                                   .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public Optional<ContractorDTO> getOne(long id) {
        logger.debug("Request to get Contractor : {}", id);

        return contractorRepository.findById(id)
                                   .map(contractor -> modelMapper.map(contractor, ContractorDTO.class));
    }

    public ContractorDTO save(ContractorDTO contractorDTO) {
        logger.debug("Request to save Contractor : {}", contractorDTO);

        Contractor contractor = modelMapper.map(contractorDTO, Contractor.class);
        Contractor saved = contractorRepository.save(contractor);
        return modelMapper.map(saved, ContractorDTO.class);
    }

    @Transactional(readOnly = true)
    public boolean existsById(long idValue) {
        logger.debug("Request to existById Contractor : {}", idValue);

        return contractorRepository.existsById(idValue);
    }

    public void delete(long id) {
        logger.debug("Request to delete Contractor : {}", id);

        contractorRepository.findById(id)
                            .ifPresent(contractorRepository::delete);
    }

    public Optional<ContractorDTO> findByNip(String nip) {
        logger.debug("Request to findByNip Contractor : {}", nip);

        return contractorRepository.findByNip(nip)
                                   .map(contractor -> modelMapper.map(contractor, ContractorDTO.class));
    }

    public Optional<ContractorDTO> findByShortName(String shortName) {
        logger.debug("Request to findByShortName Contractor : {}", shortName);

        return contractorRepository.findByShortName(shortName)
                                   .map(contractor -> modelMapper.map(contractor, ContractorDTO.class));
    }
}
