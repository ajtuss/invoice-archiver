package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InvoiceArchiverApplication {

	public static void main(String[] args) {
		SpringApplication.run(InvoiceArchiverApplication.class, args);
	}
}
