package com.example.web;

import com.example.domain.contractor.ContractorService;
import com.example.constraints.CreateEntityGroup;
import com.example.constraints.UpdateEntityGroup;
import com.example.domain.contractor.ContractorDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.groups.Default;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/api/contractors", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class ContractorController {

    private final ContractorService contractorService;
    private Logger logger = LoggerFactory.getLogger(ContractorController.class);

    @Autowired
    public ContractorController(ContractorService contractorService) {
        this.contractorService = contractorService;
    }

    @GetMapping
    public ResponseEntity<List<ContractorDTO>> getAll(Pageable pageable, @RequestParam(required = false, name = "s") String filter) {
        logger.debug("REST request to get Contractors by Name : {}", filter);

        List<ContractorDTO> result;
        if (filter == null) {
            result = contractorService.getAll();
        } else {
            result = contractorService.getAll(filter);
        }
        return ResponseEntity.ok(result);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ContractorDTO> getById(@PathVariable long id) {
        logger.debug("REST request to get Contractor : {}", id);

        Optional<ContractorDTO> contractorDTO = contractorService.getOne(id);
        return contractorDTO.map(ResponseEntity::ok)
                            .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping
    public ResponseEntity<ContractorDTO> add(@RequestBody @Validated({CreateEntityGroup.class, Default.class}) ContractorDTO contractorDTO) throws URISyntaxException {
        logger.debug("REST request to save Contractor : {}", contractorDTO);

        ContractorDTO result = contractorService.save(contractorDTO);
        return ResponseEntity.created(new URI(String.format("/api/contractors/%d", result.getId())))
                             .body(result);
    }

    @PutMapping
    public ResponseEntity<ContractorDTO> update(@RequestBody @Validated({UpdateEntityGroup.class, Default.class}) ContractorDTO contractorDTO) {
        logger.debug("REST request to update Contractor : {}", contractorDTO);

        ContractorDTO result = contractorService.save(contractorDTO);
        return ResponseEntity.ok(result);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable long id) {
        logger.debug("REST request to delete Contractor : {}", id);

        contractorService.delete(id);
        return ResponseEntity.ok()
                             .build();
    }

}
