package com.example.web;

import com.example.domain.invoice.InvoiceService;
import com.example.constraints.CreateEntityGroup;
import com.example.constraints.UpdateEntityGroup;
import com.example.domain.invoice.InvoiceDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.groups.Default;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/api/invoices", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class InvoiceController {

    private final InvoiceService invoiceService;
    private Logger logger = LoggerFactory.getLogger(InvoiceController.class);

    @Autowired
    public InvoiceController(InvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }

    @GetMapping
    public ResponseEntity<List<InvoiceDTO>> getAll() {
        logger.debug("REST request to get Invoices");

        List<InvoiceDTO> result = invoiceService.getAll();
        return ResponseEntity.ok(result);
    }

    @GetMapping("/{id}")
    public ResponseEntity<InvoiceDTO> getById(@PathVariable long id) {
        logger.debug("REST request to get Invoice : {}", id);

        Optional<InvoiceDTO> invoiceDTO = invoiceService.getOne(id);
        return invoiceDTO.map(ResponseEntity::ok)
                         .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping
    public ResponseEntity<InvoiceDTO> add(@RequestBody @Validated({CreateEntityGroup.class, Default.class}) InvoiceDTO invoiceDTO) throws URISyntaxException {
        logger.debug("REST request to save Invoice : {}", invoiceDTO);

        InvoiceDTO result = invoiceService.save(invoiceDTO);
        return ResponseEntity.created(new URI(String.format("/api/invoices/%d", result.getId())))
                             .body(result);
    }

    @PutMapping
    public ResponseEntity<InvoiceDTO> update(@RequestBody @Validated({UpdateEntityGroup.class, Default.class}) InvoiceDTO invoiceDTO) {
        logger.debug("REST request to update Invoice : {}", invoiceDTO);

        InvoiceDTO result = invoiceService.save(invoiceDTO);
        return ResponseEntity.ok(result);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable long id) {
        logger.debug("REST request to delete Invoice : {}", id);

        invoiceService.delete(id);
        return ResponseEntity.ok()
                             .build();
    }
}
