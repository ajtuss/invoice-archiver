package com.example.web;

import com.example.constraints.CreateEntityGroup;
import com.example.constraints.UpdateEntityGroup;
import com.example.domain.company.CompanyDTO;
import com.example.domain.company.CompanyService;
import com.example.domain.contractor.ContractorDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.groups.Default;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/api/companies", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class CompanyController {

    private CompanyService companyService;

    private Logger logger = LoggerFactory.getLogger(CompanyController.class);

    @Autowired
    public CompanyController(CompanyService companyService) {
        this.companyService = companyService;
    }

    @GetMapping
    public ResponseEntity<List<CompanyDTO>> getAll() {
        logger.debug("REST request to get Companies.");

        List<CompanyDTO> result = companyService.getAll();
        return ResponseEntity.ok(result);
    }

    @GetMapping("/{id}")
    public ResponseEntity<CompanyDTO> getById(@PathVariable long id) {
        logger.debug("REST request to get Company : {}", id);

        Optional<CompanyDTO> companyDTO = companyService.getOne(id);
        return companyDTO.map(ResponseEntity::ok)
                         .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping
    public ResponseEntity<CompanyDTO> add(@RequestBody @Validated({CreateEntityGroup.class, Default.class}) CompanyDTO companyDTO) throws URISyntaxException {
        logger.debug("REST request to save Company : {}", companyDTO);

        CompanyDTO result = companyService.save(companyDTO);
        return ResponseEntity.created(new URI(String.format("/api/companies/%d", result.getId())))
                             .body(result);
    }

    @PutMapping
    public ResponseEntity<CompanyDTO> update(@RequestBody @Validated({UpdateEntityGroup.class, Default.class}) CompanyDTO companyDTO) {
        logger.debug("REST request to update Company : {}", companyDTO);

        CompanyDTO result = companyService.save(companyDTO);
        return ResponseEntity.ok(result);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable long id) {
        logger.debug("REST request to delete Company : {}", id);

        companyService.delete(id);
        return ResponseEntity.ok()
                             .build();
    }

    @GetMapping("/{id}/contractors")
    public ResponseEntity<List<ContractorDTO>> getAllContractorsByCompanyId(
            @PathVariable long id,
            @RequestParam(required = false, name = "s") String filter) {

        logger.debug("REST request to get all Contractors for Company : {}", id);

        List<ContractorDTO> result = companyService.getAllContractorsByCompanyId(id, filter);

        return ResponseEntity.ok(result);
    }

    @PostMapping("/{id}/contractors")
    public ResponseEntity<Void> addContractorToCompany(@PathVariable long id, @RequestBody ContractorDTO contractorDTO) {
        logger.debug("REST request to add Contractor : {} to Company : {}", contractorDTO, id);
        boolean result = false;
        if (contractorDTO.getId() != null) {
            result = companyService.addContractorToCompany(id, contractorDTO.getId());
        } else if (contractorDTO.getShortName() != null) {
            result = companyService.addContractorToCompany(id, contractorDTO.getShortName());
        }
        return result ?
                ResponseEntity.ok()
                              .build() :
                ResponseEntity.notFound()
                              .build();
    }

    @DeleteMapping("/{id}/contractors/{contractorId}")
    public ResponseEntity<Void> deleteContractorFromCompany(@PathVariable long id, @PathVariable long contractorId) {
        logger.debug("REST request to delete Contractor : {} from Company : {}", contractorId, id);

        ContractorDTO contractorDTO = new ContractorDTO();
        contractorDTO.setId(contractorId);

        boolean result = companyService.deleteContractorFromCompany(id, contractorDTO);
        return result ?
                ResponseEntity.ok()
                              .build() :
                ResponseEntity.notFound()
                              .build();
    }
}
