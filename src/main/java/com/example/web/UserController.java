package com.example.web;

import com.example.constraints.CreateEntityGroup;
import com.example.constraints.UpdateEntityGroup;
import com.example.domain.invoice.InvoiceDTO;
import com.example.domain.user.User;
import com.example.domain.user.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.groups.Default;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/api/users", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class UserController {

    private final UserService userService;
    private Logger logger = LoggerFactory.getLogger(UserController.class);


    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public ResponseEntity<List<User>> getAll() {
        logger.debug("REST request to get Users");

        List<User> result = userService.getAll();
        return ResponseEntity.ok(result);
    }

    @GetMapping("/{id}")
    public ResponseEntity<User> getById(@PathVariable long id) {
        logger.debug("REST request to get User : {}", id);

        Optional<User> result = userService.getOne(id);
        return result.map(ResponseEntity::ok)
                         .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping
    public ResponseEntity<User> add(@RequestBody @Validated({CreateEntityGroup.class, Default.class}) User user) throws URISyntaxException {
        logger.debug("REST request to save User : {}", user);

        User result = userService.save(user);
        return ResponseEntity.created(new URI(String.format("/api/users/%d", result.getId())))
                             .body(result);
    }

    @PutMapping
    public ResponseEntity<User> update(@RequestBody @Validated({UpdateEntityGroup.class, Default.class}) User user) {
        logger.debug("REST request to update User : {}", user);

        User result = userService.save(user);
        return ResponseEntity.ok(result);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable long id) {
        logger.debug("REST request to delete Invoice : {}", id);

        userService.delete(id);
        return ResponseEntity.ok()
                             .build();
    }

}
